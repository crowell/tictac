#include <iostream>
#include <cstdlib>
#include <algorithm>
#include "player.h"
#include "exPlayer.h"
#define size 10
#define boardSize 10

// auxilliary functions
inline int min(int a, int b)
{
    if (a<b) return a;
    else return b;
}

inline int max(int a, int b)
{
    if (a<b) return b;
    else return a;
}

exPlayer::exPlayer(square **initTable)
{
    myColor=ex;
    ptr = 0;
    table = new int*[size];
    for (int ii=0; ii<size; ii++)
        table[ii]=new int[size];
    for (int ii=0; ii<size; ii++)
        for (int jj=0; jj<size; jj++)
            table[ii][jj]=initTable[ii][jj];
}

void exPlayer::otherMove(boardSquare bs)
{
    table[bs.xx][bs.yy]=(myColor==ex?oh:ex);
}

boardSquare exPlayer::nextMove()
{
    boardSquare bs;
	boardSquare xWin = exWinner();
	if(xWin.xx != -1 && xWin.yy != -1)
	{
		if(table[xWin.xx][xWin.yy] == blank )
		{
			table[xWin.xx][xWin.yy]=ex;
			return xWin;
		}
	}
	else
	{
		xWin = ohWinner();
		if(xWin.xx != -1 && xWin.yy != -1)
		{
			if(table[xWin.xx][xWin.yy] == blank) //&& table[xWin.xx][xWin.yy] != unusable )
			{
				table[xWin.xx][xWin.yy]=ex;
				return xWin;
			}
		}
				xWin = exTry();
		if(xWin.xx != -1 && xWin.yy != -1)
		{
			if(table[xWin.xx][xWin.yy] == blank) //&& table[xWin.xx][xWin.yy] != unusable )
			{
				table[xWin.xx][xWin.yy]=ex;
				return xWin;
			}
		}
				xWin = ohTry();
		if(xWin.xx != -1 && xWin.yy != -1)
		{
			if(table[xWin.xx][xWin.yy] == blank) //&& table[xWin.xx][xWin.yy] != unusable )
			{
				table[xWin.xx][xWin.yy]=ex;
				return xWin;
			}
		}
		else
		{
			while (table[bs.xx][bs.yy]!=blank)
			{
				bs.yy = (boardSize  - (ptr%boardSize) )%boardSize;
				bs.xx = boardSize - 1  - ((ptr/boardSize)%boardSize);
				ptr++;
			}
			table[bs.xx][bs.yy]=ex;
			return bs;
		}
	}
}
boardSquare exPlayer::exTry()
{
	boardSquare none;
	none.xx = -1;
	none.yy = -1;
	boardSquare blankSquare;
		//first check out the Horiz. Winners
	for(int ii = 0; ii < size; ii++)
	{
		for(int jj = 0; jj < size - 5; jj++)
		{
			int numx = 0;
			int numblank = 0;
			for(int kk = 0; kk < 5; kk++)
			{
				if(table[jj+kk][ii] == blank)
				{
					numblank++;
					blankSquare.xx = jj+kk;
					blankSquare.yy = ii;
				}
				else if(table[jj+kk][ii] == ex)
				{
					numx++;
				}
			}
			if(numx == 3 && numblank == 2)
			{
				return blankSquare;
			}
		}
	}
	//now check for Vertical Winners
	for(int ii = 0; ii < size; ii++)
	{
		for(int jj = 0; jj < size - 5; jj++)
		{
			int numx = 0;
			int numblank = 0;
			for(int kk = 0; kk < 5; kk++)
			{
				if(table[ii][jj+kk] == blank)
				{
					numblank++;
					blankSquare.xx = ii;
					blankSquare.yy = jj+kk;
				}
				else if(table[ii][jj+kk] == ex)
				{
					numx++;
				}
			}
			if(numx == 2 && numblank == 3)
			{
				return blankSquare;
			}
		}
	}
			//first check out the Horiz. Winners
	for(int ii = 0; ii < size; ii++)
	{
		for(int jj = 0; jj < size - 5; jj++)
		{
			int numx = 0;
			int numblank = 0;
			for(int kk = 0; kk < 5; kk++)
			{
				if(table[jj+kk][ii] == blank)
				{
					numblank++;
					blankSquare.xx = jj+kk;
					blankSquare.yy = ii;
				}
				else if(table[jj+kk][ii] == ex)
				{
					numx++;
				}
			}
			if(numx == 2 && numblank == 3)
			{
				return blankSquare;
			}
		}
	}
	//now check for Vertical Winners
	for(int ii = 0; ii < size; ii++)
	{
		for(int jj = 0; jj < size - 5; jj++)
		{
			int numx = 0;
			int numblank = 0;
			for(int kk = 0; kk < 5; kk++)
			{
				if(table[ii][jj+kk] == blank)
				{
					numblank++;
					blankSquare.xx = ii;
					blankSquare.yy = jj+kk;
				}
				else if(table[ii][jj+kk] == ex)
				{
					numx++;
				}
			}
			if(numx == 2 && numblank == 3)
			{
				return blankSquare;
			}
		}
	}
}
boardSquare exPlayer::exWinner()
{
	boardSquare none;
	none.xx = -1;
	none.yy = -1;
	boardSquare blankSquare;
	//return none;
	//first check out the Horiz. Winners
	for(int ii = 0; ii < size; ii++)
	{
		for(int jj = 0; jj < size - 5; jj++)
		{
			int numx = 0;
			int numblank = 0;
			for(int kk = 0; kk < 5; kk++)
			{
				if(table[jj+kk][ii] == blank)
				{
					numblank++;
					blankSquare.xx = jj+kk;
					blankSquare.yy = ii;
				}
				else if(table[jj+kk][ii] == ex)
				{
					numx++;
				}
			}
			if(numx == 4 && numblank == 1)
			{
				return blankSquare;
			}
		}
	}
	//now check for Vertical Winners
	for(int ii = 0; ii < size; ii++)
	{
		for(int jj = 0; jj < size - 5; jj++)
		{
			int numx = 0;
			int numblank = 0;
			for(int kk = 0; kk < 5; kk++)
			{
				if(table[ii][jj+kk] == blank)
				{
					numblank++;
					blankSquare.xx = ii;
					blankSquare.yy = jj+kk;
				}
				else if(table[ii][jj+kk] == ex)
				{
					numx++;
				}
			}
			if(numx == 4 && numblank == 1)
			{
				return blankSquare;
			}
		}
	}
		//now check for diagonal in a more naieve way
	int ii;
	int jj;
	int streak=1;
	int diff;
	boardSquare bs;
	for (diff=-boardSize+5; diff<=boardSize-5; diff++, streak=1)
		for (jj=max(1-diff,1); jj<min(boardSize-diff,boardSize); jj++)
			if ((table[jj][jj+diff]==ex) && (table[jj][jj+diff]==table[jj-1][jj+diff-1]))
				{
					streak++;
					if ((streak==4) && ((jj-4) >=0) && ((jj+diff-4)>=0) && (table[jj-4][jj+diff-4]==blank) ) // *\ winner
					{
						bs.yy = jj+diff-4;
						bs.xx = jj-4;
						return bs;

					}
					else if ((streak==4) && ((jj+1)<=9) && ((jj+diff+1)<=9) && table[jj+1][jj+diff+1]==blank) // \* winner
					{
						bs.yy = jj+diff+1;
						bs.xx = jj+1;
						return bs;
					}
				}
			else
			{
			  streak=1;
			}
			
	//now check the other direction diagonal
		streak=1;
	int sum;
	for (sum=4; (sum<2*boardSize-5); sum++, streak=1)
		for (jj=max(1,sum-boardSize+2); jj<min(sum+1,boardSize); jj++)
			if ((table[jj][sum-jj]==ex) && (table[jj][sum-jj]==table[jj-1][sum-jj+1]))
			{
				streak++;
				if ((streak==4) && ((jj-4)>=0) && ((sum-jj+4)>=0) && table[jj-4][sum-jj+4]==blank)// /* winner
				{
					bs.yy = sum-jj+4;
					bs.xx = jj-4;
					return bs;
				}
				else if ((streak ==4)  && ((jj+1)<=9) && ((sum-jj-1)<=9) && table[jj+1][sum-jj-1]==blank)// */ winner
				{
						bs.yy = sum-jj-1;
						bs.xx = jj+1;
						return bs;

				}
			}
			else
			{
			  streak=1;
			}

	return none;
}

boardSquare exPlayer::ohTry()
{
		boardSquare none;
	none.xx = -1;
	none.yy = -1;
	boardSquare blankSquare;
    	//first check out the Horiz. Winners
	for(int ii = 0; ii < size; ii++)
	{
		for(int jj = 0; jj < size - 5; jj++)
		{
			int numx = 0;
			int numblank = 0;
			for(int kk = 0; kk < 5; kk++)
			{
				if(table[jj+kk][ii] == blank)
				{
					numblank++;
					blankSquare.xx = jj+kk;
					blankSquare.yy = ii;
				}
				else if(table[jj+kk][ii] == oh)
				{
					numx++;
				}
			}
			if(numx == 3 && numblank == 2)
			{
				return blankSquare;
			}
		}
	}
	//now check for Vertical Winners
	for(int ii = 0; ii < size; ii++)
	{
		for(int jj = 0; jj < size - 5; jj++)
		{
			int numx = 0;
			int numblank = 0;
			for(int kk = 0; kk < 5; kk++)
			{
				if(table[ii][jj+kk] == blank)
				{
					numblank++;
					blankSquare.xx = ii;
					blankSquare.yy = (jj+kk);
				}
				else if(table[ii][jj+kk] == oh)
				{
					numx++;
				}
			}
			if(numx == 3 && numblank == 2)
			{
				return blankSquare;
			}
		}
	}
	    	//first check out the Horiz. Winners
	for(int ii = 0; ii < size; ii++)
	{
		for(int jj = 0; jj < size - 5; jj++)
		{
			int numx = 0;
			int numblank = 0;
			for(int kk = 0; kk < 5; kk++)
			{
				if(table[jj+kk][ii] == blank)
				{
					numblank++;
					blankSquare.xx = jj+kk;
					blankSquare.yy = ii;
				}
				else if(table[jj+kk][ii] == oh)
				{
					numx++;
				}
			}
			if(numx == 2 && numblank == 3)
			{
				return blankSquare;
			}
		}
	}
	//now check for Vertical Winners
	for(int ii = 0; ii < size; ii++)
	{
		for(int jj = 0; jj < size - 5; jj++)
		{
			int numx = 0;
			int numblank = 0;
			for(int kk = 0; kk < 5; kk++)
			{
				if(table[ii][jj+kk] == blank)
				{
					numblank++;
					blankSquare.xx = ii;
					blankSquare.yy = (jj+kk);
				}
				else if(table[ii][jj+kk] == oh)
				{
					numx++;
				}
			}
			if(numx == 2 && numblank == 3)
			{
				return blankSquare;
			}
		}
	}
	return none;
}

boardSquare exPlayer::ohWinner()
{
	boardSquare none;
	none.xx = -1;
	none.yy = -1;
	boardSquare blankSquare;
	//return none;
	//first check out the Horiz. Winners
	for(int ii = 0; ii < size; ii++)
	{
		for(int jj = 0; jj < size - 5; jj++)
		{
			int numx = 0;
			int numblank = 0;
			for(int kk = 0; kk < 5; kk++)
			{
				if(table[jj+kk][ii] == blank)
				{
					numblank++;
					blankSquare.xx = jj+kk;
					blankSquare.yy = ii;
				}
				else if(table[jj+kk][ii] == oh)
				{
					numx++;
				}
			}
			if(numx == 4 && numblank == 1)
			{
				return blankSquare;
			}
		}
	}
	//now check for Vertical Winners
	for(int ii = 0; ii < size; ii++)
	{
		for(int jj = 0; jj < size - 5; jj++)
		{
			int numx = 0;
			int numblank = 0;
			for(int kk = 0; kk < 5; kk++)
			{
				if(table[ii][jj+kk] == blank)
				{
					numblank++;
					blankSquare.xx = ii;
					blankSquare.yy = (jj+kk);
				}
				else if(table[ii][jj+kk] == oh)
				{
					numx++;
				}
			}
			if(numx == 4 && numblank == 1)
			{
				return blankSquare;
			}
		}
	}
	//now check for diagonal in a more naieve way
	int ii;
	int jj;
	int streak=1;
	int diff;
	boardSquare bs;
	for (diff=-boardSize+5; diff<=boardSize-5; diff++, streak=1)
		for (jj=max(1-diff,1); jj<min(boardSize-diff,boardSize); jj++)
			if ((table[jj][jj+diff]==oh) && (table[jj][jj+diff]==table[jj-1][jj+diff-1]))
				{
					streak++;
					if ((streak==4) && ((jj-4) >=0) && ((jj+diff-4)>=0) && (table[jj-4][jj+diff-4]==blank) ) // *\ winner
					{
						bs.yy = jj+diff-4;
						bs.xx = jj-4;
						//table[bs.xx][bs.yy] = oh;
						return bs;

					}
					else if ((streak==4) && ((jj+1)<=9) && ((jj+diff+1)<=9) && table[jj+1][jj+diff+1]==blank) // \* winner
					{
						bs.yy = jj+diff+1;
						bs.xx = jj+1;
						//table[bs.xx][bs.yy] = oh;
						return bs;// we have a winning move on bottom right
					}
				}
			else
			{
			  streak=1;
			}
			
	//now check the other direction diagonal
		streak=1;
	int sum;
	for (sum=4; (sum<2*boardSize-5); sum++, streak=1)
		for (jj=max(1,sum-boardSize+2); jj<min(sum+1,boardSize); jj++)
			if ((table[jj][sum-jj]==oh) && (table[jj][sum-jj]==table[jj-1][sum-jj+1]))
			{
				streak++;
				if ((streak==4) && ((jj-4)>=0) && ((sum-jj+4)>=0) && table[jj-4][sum-jj+4]==blank)// /* winner
				{
					bs.yy = sum-jj+4;
					bs.xx = jj-4;
					table[bs.xx][bs.yy] = oh;
					return bs;
				}
				else if ((streak ==4)  && ((jj+1)<=9) && ((sum-jj-1)<=9) && table[jj+1][sum-jj-1]==blank)// */ winner
				{
						bs.yy = sum-jj-1;
						bs.xx = jj+1;
						return bs;

				}
			}
			else
			{
			  streak=1;
			}
return none;
}
